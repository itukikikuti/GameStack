﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameStack.Demo
{
    public class UIDemo : MonoBehaviour
    {
        private class State : UI.State
        {
        }

        private class Actions : UI.Actions
        {
        }

        private Inventory<Item> inventory = new Inventory<Item>(10);
        [SerializeField]
        private Transform cube = null;
        [SerializeField, HideInInspector]
        private float scroll = 0f;

        private UI.View view = (uiState, uiActions) =>
        {
            State state = uiState as State;
            Actions actions = uiActions as Actions;

            return UI.Root(
                UI.PaddingLayout(
                    UI.GridLayout(
                        Enumerable.Range(0, 10).Select(index =>
                        {
                            return UI.Button(
                                UI.Image(
                                    UI.Text($"item {index}")
                                ),
                                () => Transition.LoadScene("CharacterDemo", 3f)
                            );
                        }).ToArray(),
                        new Vector2(800f / 10f, 800f / 10f),
                        new Vector2(5f, 5f)
                    ),
                    5f, 5f, 5f, 5f
                )
            );
        };

        private void Start()
        {
            State state = new State();
            Actions actions = new Actions();
            new UI(view, state, actions);
        }

        private void Update()
        {
            if (NativeInput.GetMouseWheel() != 0f)
                scroll = NativeInput.GetMouseWheel() * 100f;

            scroll *= 0.9f;
            cube.transform.Rotate(scroll, 0f, 0f);
        }
    }
}
