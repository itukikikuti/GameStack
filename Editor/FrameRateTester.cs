﻿using UnityEditor;
using UnityEngine;

namespace GameStack.Editor
{
    public static class FrameRateTester
    {
        [MenuItem("Edit/Frame Rate/Default")]
        private static void SetDefaultFrameRate()
        {
            Application.targetFrameRate = -1;
            QualitySettings.vSyncCount = 1;
        }

        [MenuItem("Edit/Frame Rate/Maximum")]
        private static void SetMaximumFrameRate()
        {
            Application.targetFrameRate = -1;
            QualitySettings.vSyncCount = 0;
        }

        [MenuItem("Edit/Frame Rate/60")]
        private static void Set60FrameRate()
        {
            Application.targetFrameRate = 60;
            QualitySettings.vSyncCount = 0;
        }

        [MenuItem("Edit/Frame Rate/30")]
        private static void Set30FrameRate()
        {
            Application.targetFrameRate = 30;
            QualitySettings.vSyncCount = 0;
        }

        [MenuItem("Edit/Frame Rate/10")]
        private static void Set10FrameRate()
        {
            Application.targetFrameRate = 10;
            QualitySettings.vSyncCount = 0;
        }
    }
}
