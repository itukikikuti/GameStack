﻿using UnityEditor;
using UnityEngine;

namespace GameStack.Editor
{
    public static class TemplateScriptCreator
    {
        [MenuItem("Assets/Create/Script", priority = 79)]
        private static void Create()
        {
            TextAsset asset = new TextAsset("aaa");
            AssetDatabase.CreateAsset(asset, "Assets/aaa.cs");
        }
    }
}
