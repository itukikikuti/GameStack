﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStack
{
    public static class Audio
    {
        private class AudioPlayer : SingletonBehaviour<AudioPlayer>
        {
            private List<AudioSource> audioSources = new List<AudioSource>();

            public AudioSource Play(AudioClip clip, float volume, float pitch)
            {
                AudioSource audioSource = null;

                foreach (AudioSource temp in audioSources)
                {
                    if (!temp.isPlaying)
                    {
                        audioSource = temp;
                        break;
                    }
                }

                if (audioSource == null)
                {
                    audioSource = gameObject.AddComponent<AudioSource>();
                    audioSources.Add(audioSource);
                }

                audioSource.clip = clip;
                audioSource.volume = volume;
                audioSource.pitch = pitch;

                audioSource.Play();

                return audioSource;
            }
        }

        public static AudioSource Play(AudioClip clip)
        {
            return AudioPlayer.Instance.Play(clip, 1f, 1f);
        }

        public static AudioSource Play(AudioClip clip, float volume)
        {
            return AudioPlayer.Instance.Play(clip, volume, 1f);
        }

        public static AudioSource Play(AudioClip clip, float volume, float pitch)
        {
            return AudioPlayer.Instance.Play(clip, volume, pitch);
        }
    }
}
