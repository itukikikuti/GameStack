﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameStack
{
    [RequireComponent(typeof(Collider))]
    public class KinematicBody : MonoBehaviour
    {
        private class Contacts : MonoBehaviour, IEnumerable<ContactPoint>
        {
            [SerializeField]
            private List<ContactPoint> contacts = new List<ContactPoint>();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public IEnumerator<ContactPoint> GetEnumerator()
            {
                foreach (var contact in contacts)
                {
                    yield return contact;
                }
            }

            private void FixedUpdate()
            {
                contacts.Clear();
            }

            private void OnCollisionStay(Collision collision)
            {
                contacts.AddRange(collision.contacts);
            }
        }

        [SerializeField, HideInInspector]
        private new Rigidbody rigidbody = null;
        [SerializeField, HideInInspector]
        private new Collider collider = null;
        [SerializeField, HideInInspector]
        private Contacts contacts = null;

        public Vector2 Velocity
        {
            get;
            set;
        }

        [SerializeField]
        private float maxVelocity = 10f;
        public float MaxVelocity
        {
            get => maxVelocity;
            set => maxVelocity = value;
        }

        [SerializeField]
        private float friction = 0.1f;
        public float Friction
        {
            get => friction;
            set => friction = value;
        }

        [SerializeField]
        private float maxSlopeAngle = 45f;
        public float MaxSlopeAngle
        {
            get => maxSlopeAngle;
            set => maxSlopeAngle = value;
        }

        [SerializeField, NotEditable]
        private bool isGrounded = false;
        public bool IsGrounded
        {
            get => isGrounded;
            private set => isGrounded = value;
        }

        private void Awake()
        {
            rigidbody = gameObject.ForceGetComponent<Rigidbody>();
            rigidbody.hideFlags = HideFlags.HideInInspector | HideFlags.DontSave;
            rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

            collider = gameObject.GetComponent<Collider>();
            collider.material = new PhysicMaterial
            {
                bounciness = 0f,
                bounceCombine = PhysicMaterialCombine.Maximum,
                dynamicFriction = 0f,
                staticFriction = 0f,
                frictionCombine = PhysicMaterialCombine.Maximum,
            };

            contacts = gameObject.AddComponent<Contacts>();
            contacts.hideFlags = HideFlags.HideInInspector | HideFlags.DontSave;
        }

        private void Update()
        {
#if UNITY_EDITOR
            foreach (var contact in contacts)
            {
                GizmoRenderer.DrawLine(contact.point, contact.point + contact.normal * 0.1f, Color.red);
            }
#endif

            var groundContacts = contacts.Where(c => c.normal.y >= Mathf.Cos(MaxSlopeAngle * Mathf.Deg2Rad));
            var wallContacts = contacts.Where(c => c.normal.y < Mathf.Cos(MaxSlopeAngle * Mathf.Deg2Rad));
            IsGrounded = groundContacts.Count() > 0;

            if (IsGrounded)
            {
                // 摩擦の処理で、FixedUpdateだと1秒あたりの処理回数がちゃんと一定になる。
                // Updateだと処理回数が変動しちゃうので、摩擦が強くなったり弱くなったりする。
                // なのでLerpの引数tを補正しなきゃいけない。
                // 例えばFixedUpdateが1秒に50回呼ばれるとしたら、FixedUpdateで何かの値に+1するとすると
                // FixedUpdateは1秒で50回呼ばれるので、+50になる。
                // Updateで考えると、+1*Time.deltaTimeすると1秒で+1になるので、50かければいい。
                // 50はFixedUpdateが1秒間に呼ばれる回数なので1/Time.fixedDeltaTimeで計算できる。
                // つまり、t * Time.deltaTime * (1f / Time.fixedDeltaTime)で補正できる。
                Velocity = Vector2.Lerp(Velocity, Vector2.zero, Friction * Time.deltaTime * (1f / Time.fixedDeltaTime));
                Velocity = Vector2.ClampMagnitude(Velocity, MaxVelocity);
            }

            Vector3 velocity3 = new Vector3(Velocity.x, 0f, Velocity.y);

            foreach (var contact in wallContacts)
            {
                Vector3 normal = contact.normal;
                normal.y = 0f;
                if (Vector3.Dot(velocity3, normal.normalized) > 0f)
                    continue;

                velocity3 = Vector3.ProjectOnPlane(velocity3, normal.normalized);
            }

            //if (IsGrounded)
            //{
            //    Vector3 projectedVelocity = Vector3.ProjectOnPlane(velocity3, groundContacts.First().normal);
            //    rigidbody.velocity = projectedVelocity;
            //}
            //else
            {
                velocity3.y = rigidbody.velocity.y;
                rigidbody.velocity = velocity3;
            }

            Velocity = new Vector2(velocity3.x, velocity3.z);
        }

        public void Jump(float force)
        {
            rigidbody.velocity = new Vector3(rigidbody.velocity.x, force, rigidbody.velocity.z);
        }
    }
}
