﻿using System;
using UnityEngine;

namespace GameStack
{
    [RequireComponent(typeof(KinematicBody))]
    public class ThirdPersonController : MonoBehaviour
    {
        [SerializeField, HideInInspector]
        private KinematicBody kinematicBody = null;
        [SerializeField]
        private new Camera camera = null;
        [SerializeField]
        private float moveForce = 100f;
        [SerializeField]
        private float jumpForce = 5f;
        [SerializeField]
        private Vector2 cameraOffset = Vector2.zero;
        [SerializeField]
        private float cameraSensity = 10f;
        [SerializeField]
        private float cameraMaxDistance = 5f;
        [SerializeField]
        private float cameraAngleMin = -80f;
        [SerializeField]
        private float cameraAngleMax = 50f;
        [SerializeField, HideInInspector]
        private Vector3 cameraAngles;
        [SerializeField, HideInInspector]
        private Quaternion cameraRotation;

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            kinematicBody = gameObject.GetComponent<KinematicBody>();

            cameraAngles = transform.eulerAngles + new Vector3(0f, 180f, 0f);
            cameraRotation = Quaternion.Euler(cameraAngles);
        }

        private void Update()
        {
            if (kinematicBody.IsGrounded)
            {
                Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
                Vector3 turnDirection = camera.transform.forward.Convert((x, y, z) => new Vector3(x, 0f, z).normalized);

                if (moveDirection.x != 0f || moveDirection.z != 0f)
                {
                    transform.rotation = Quaternion.LookRotation(turnDirection) * Quaternion.LookRotation(moveDirection);
                    kinematicBody.Velocity += transform.forward.Convert((x, _, z) => new Vector2(x, z)) * Time.deltaTime * moveForce;
                }

                if (Input.GetButtonDown("Jump"))
                    kinematicBody.Jump(jumpForce);
            }

            cameraAngles += new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f) * cameraSensity;
            View(transform.position, cameraAngleMin, cameraAngleMax);
        }

        private void View(Vector3 origin, float angleMin = -90f, float angleMax = 90f)
        {
            angleMin = Mathf.Max(angleMin, -89.9f);
            angleMax = Mathf.Min(angleMax, 89.9f);

            cameraAngles.x = Mathf.Clamp(cameraAngles.x, angleMin, angleMax);
            cameraRotation = Quaternion.Euler(cameraAngles);
            //rotation = Quaternion.Lerp(rotation, Quaternion.Euler(cameraAngles), Time.deltaTime * 50f);
            Vector3 direction = cameraRotation * Vector3.forward;

            float distance = cameraMaxDistance;

            origin += (cameraRotation * Vector3.left) * cameraOffset.x + (cameraRotation * Vector3.up) * cameraOffset.y;

            RaycastHit hit;
            if (Physics.SphereCast(origin, camera.nearClipPlane, direction, out hit, cameraMaxDistance))
            {
                distance = hit.distance;
            }
            if (Physics.CheckSphere(origin, camera.nearClipPlane, gameObject.layer))
            {
                distance = 0f;
            }

            camera.transform.position = origin + direction * distance;
            camera.transform.rotation = Quaternion.LookRotation(-direction);
        }

    }
}
