﻿using System;
using UnityEngine;

namespace GameStack
{
    public static class ComponentCacheExtensions
    {
        public static T ForceGetComponent<T>(this GameObject gameObject)
            where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component != null) return component;

            return gameObject.AddComponent<T>();
        }

        [Obsolete]
        public static ComponentCache<T> AddComponentCache<T>(this GameObject gameObject)
            where T : Component
        {
            return new ComponentCache<T>(() => gameObject.AddComponent<T>());
        }

        [Obsolete]
        public static ComponentCache<T> GetComponentCache<T>(this GameObject gameObject)
            where T : Component
        {
            return new ComponentCache<T>(() => gameObject.GetComponent<T>());
        }

        [Obsolete]
        public static ComponentCache<T> ForceGetComponentCache<T>(this GameObject gameObject)
            where T : Component
        {
            return new ComponentCache<T>(() => gameObject.ForceGetComponent<T>());
        }
    }

    [Obsolete]
    public class ComponentCache<T>
        where T : Component
    {
        private T component = null;
        private Func<T> initializer = null;

        public ComponentCache(Func<T> initializer)
        {
            this.initializer = initializer;
        }

        public T Cache
        {
            get
            {
                if (component == null)
                    component = initializer();

                return component;
            }
        }
    }
}
