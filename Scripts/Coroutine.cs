﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameStack
{
    public class CoroutineRunner : SingletonBehaviour<CoroutineRunner>
    {
        public static Coroutine RunCoroutine(IEnumerator coroutine)
        {
            return Instance.StartCoroutine(coroutine);
        }
    }

    public static class AwaitExtension
    {
        public class CoroutineAwaiter : INotifyCompletion
        {
            private Action continuation = null;
            public bool IsCompleted { get; private set; } = false;

            public void OnCompleted(Action continuation)
            {
                this.continuation = continuation;
            }

            public void EnforceComplete()
            {
                IsCompleted = true;
                continuation?.Invoke();
            }

            public void GetResult()
            {
            }
        }

        public static CoroutineAwaiter GetAwaiter(this YieldInstruction self)
        {
            CoroutineAwaiter awaiter = new CoroutineAwaiter();
            CoroutineRunner.RunCoroutine(WaitCoroutine(awaiter, self));
            return awaiter;
        }

        public static CoroutineAwaiter GetAwaiter(this IEnumerator self)
        {
            CoroutineAwaiter awaiter = new CoroutineAwaiter();
            CoroutineRunner.RunCoroutine(WaitCoroutine(awaiter, self));
            return awaiter;
        }

        public static CoroutineAwaiter GetAwaiter(this WaitForNextFrame self)
        {
            CoroutineAwaiter awaiter = new CoroutineAwaiter();
            CoroutineRunner.RunCoroutine(WaitForNextFrame(awaiter));
            return awaiter;
        }

        private static IEnumerator WaitCoroutine(CoroutineAwaiter awaiter, object coroutine)
        {
            yield return coroutine;
            awaiter.EnforceComplete();
        }

        private static IEnumerator WaitForNextFrame(CoroutineAwaiter awaiter)
        {
            yield return null;
            awaiter.EnforceComplete();
        }
    }

    // 型だけ定義して挙動は上で実装しているただのダミー
    public class WaitForNextFrame
    {
    }
}
