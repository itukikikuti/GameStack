﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameStack
{
    [Serializable]
    public class Inventory<T> : IEnumerable<Inventory<T>.Data> where T : Item
    {
        [Serializable]
        public struct Data
        {
            [SerializeField]
            public T item;
            [SerializeField]
            public uint count;
        }

        [SerializeField]
        private Data[] datas = null;

        public Inventory(int capacity)
        {
            datas = new Data[capacity];
        }

        public bool CanAddItem(T item)
        {
            foreach (var data in datas)
            {
                if (data.item == null)
                    return true;

                if (data.item == item && data.count < item.stackLimit)
                    return true;
            }

            return false;
        }

        public void AddItem(T item)
        {
            for (int i = 0; i < datas.Length; i++)
            {
                if (datas[i].item == null)
                {
                    datas[i].item = item;
                    datas[i].count = 1;
                    return;
                }

                if (datas[i].item == item && datas[i].count < item.stackLimit)
                {
                    datas[i].count++;
                    return;
                }
            }
        }

        public void RemoveItem(T item)
        {
            for (int i = 0; i < datas.Length; i++)
            {
                if (datas[i].item == item)
                {
                    datas[i].count--;

                    if (datas[i].count <= 0)
                        datas[i].item = null;

                    return;
                }
            }
        }

        public IEnumerator<Data> GetEnumerator()
        {
            foreach (var data in datas)
            {
                yield return data;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
