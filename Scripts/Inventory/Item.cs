﻿using UnityEngine;

namespace GameStack
{
    [CreateAssetMenu(menuName = "Item/Item")]
    public class Item : ScriptableObject
    {
        [SerializeField]
        public uint stackLimit = 999;
    }
}
