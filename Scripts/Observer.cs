﻿using System;
using UnityEngine;

namespace GameStack
{
    public class Observer<T>
    {
        private Action<T> onNext;

        public Observer(Action<T> onNext)
        {
            this.onNext = onNext;
        }

        public void OnNext(T value)
        {
            onNext(value);
        }
    }

    public class Observable<T>
    {
        private Action<Observer<T>> subscribe;

        public Observable(Action<Observer<T>> subscribe)
        {
            this.subscribe = subscribe;
        }
        public void Subscribe(Observer<T> observer)
        {
            subscribe(observer);
        }
    }


    public class ObservableBehaviour : MonoBehaviour
    {
        Observable<GameObject> observableUpdate;
        Observer<GameObject> updateObserver;

        private void Update()
        {
            if (observableUpdate != null) updateObserver.OnNext(gameObject);
        }

        public Observable<GameObject> ObservableUpdate()
        {
            if (observableUpdate == null)
                observableUpdate = new Observable<GameObject>(o => updateObserver = o);

            return observableUpdate;
        }

        Observable<GameObject> observableDestroy;
        Observer<GameObject> destroyObserver;

        private void OnDestroy()
        {
            if (observableDestroy != null) destroyObserver.OnNext(gameObject);
        }

        public Observable<GameObject> ObservableDestroy()
        {
            if (observableDestroy == null)
                observableDestroy = new Observable<GameObject>(o => destroyObserver = o);

            return observableDestroy;
        }
    }

    public static class RxEx
    {
        public static void Subscribe<T>(this Observable<T> observable, Action<T> subscribe)
        {
            observable.Subscribe(new Observer<T>(value => subscribe(value)));
        }

        public static Observable<GameObject> OnUpdate(this GameObject gameObject)
        {
            ObservableBehaviour temp = gameObject.GetComponent<ObservableBehaviour>();

            if (temp == null)
                temp = gameObject.AddComponent<ObservableBehaviour>();

            return temp.ObservableUpdate();
        }

        public static Observable<GameObject> OnDestroy(this GameObject gameObject)
        {
            ObservableBehaviour temp = gameObject.GetComponent<ObservableBehaviour>();

            if (temp == null)
                temp = gameObject.AddComponent<ObservableBehaviour>();

            return temp.ObservableDestroy();
        }
    }
}
