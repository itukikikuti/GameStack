﻿using System;
using UnityEngine;

namespace GameStack
{
    public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance = null;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject go = new GameObject(typeof(T).Name);
                    instance = go.AddComponent<T>();
                }

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
                instance = GetComponent<T>();

            gameObject.hideFlags = HideFlags.DontSave;
            DontDestroyOnLoad(gameObject);
        }
    }

    public abstract class SingletonPrefab<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance = null;
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject go = Instantiate(Resources.LoadAll<T>("")[0].gameObject);
                    instance = go.GetComponent<T>();
                }

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
                instance = GetComponent<T>();

            gameObject.hideFlags = HideFlags.DontSave;
            DontDestroyOnLoad(gameObject);
        }
    }
}
