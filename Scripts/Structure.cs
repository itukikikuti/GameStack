﻿using System;
using System.Collections;
using UnityEngine;

namespace GameStack
{
    public static class VectorExtensions
    {
        public class WrappedVector3
        {
            public Vector3 vector3;

            public float x { get => vector3.x; set => vector3.x = value; }
            public float y { get => vector3.y; set => vector3.y = value; }
            public float z { get => vector3.z; set => vector3.z = value; }
        }

        public static void SetPosition(this Transform self, Action<WrappedVector3> setter)
        {
            WrappedVector3 temp = new WrappedVector3();
            temp.vector3 = self.position;
            setter(temp);
            self.position = temp.vector3;
        }

        public static void SetLocalPosition(this Transform self, Action<WrappedVector3> setter)
        {
            WrappedVector3 temp = new WrappedVector3();
            temp.vector3 = self.localPosition;
            setter(temp);
            self.localPosition = temp.vector3;
        }

        public static void SetEulerAngles(this Transform self, Action<WrappedVector3> setter)
        {
            WrappedVector3 temp = new WrappedVector3();
            temp.vector3 = self.eulerAngles;
            setter(temp);
            self.eulerAngles = temp.vector3;
        }

        public static void SetLocalEulerAngles(this Transform self, Action<WrappedVector3> setter)
        {
            WrappedVector3 temp = new WrappedVector3();
            temp.vector3 = self.localEulerAngles;
            setter(temp);
            self.localEulerAngles = temp.vector3;
        }

        public static void SetLocalScale(this Transform self, Action<WrappedVector3> setter)
        {
            WrappedVector3 temp = new WrappedVector3();
            temp.vector3 = self.localScale;
            setter(temp);
            self.localScale = temp.vector3;
        }

        public static T Convert<T>(this Vector3 self, Func<float, float, float, T> setter)
        {
            var (x, y, z) = self;
            return setter(x, y, z);
        }

        public static void Deconstruct(this Vector3 a, out float x, out float y, out float z)
        {
            x = a.x;
            y = a.y;
            z = a.z;
        }
    }

    public enum Orientation
    {
        Up,
        Right,
        Down,
        Left
    }
}
