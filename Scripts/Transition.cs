﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameStack
{
    public static class Transition
    {
        internal class Transitioner : SingletonBehaviour<Transitioner>
        {
            public Image image = null;

            public static Coroutine TweenAlpha(float alpha, float sec, EaseType easeType = EaseType.Linear)
            {
                return Instance.image.TweenAlpha(alpha, sec, easeType);
            }

            private void Awake()
            {
                Canvas canvas = gameObject.AddComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;

                image = gameObject.AddComponent<Image>();
                image.color = new Color(0f, 0f, 0f, 0f);
            }
        }

        public static void LoadScene(string sceneName, float sec, Color? color = null, EaseType easeType = EaseType.Linear)
        {
            RunLoadScene(sceneName, sec / 2f, color ?? Color.black, easeType);
        }

        private static async void RunLoadScene(string sceneName, float sec, Color color, EaseType easeType = EaseType.Linear)
        {
            color.a = 0f;
            Transitioner.Instance.image.color = color;

            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);
            asyncOperation.allowSceneActivation = false;

            await Transitioner.TweenAlpha(1f, sec, easeType);

            await new WaitWhile(() => asyncOperation.progress < 0.9f);
            asyncOperation.allowSceneActivation = true;

            await Transitioner.TweenAlpha(0f, sec, easeType);
        }
    }
}
