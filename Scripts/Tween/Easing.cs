﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStack
{
    public static class Easing
    {
        public static float Linear(float a, float b, float t)
        {
            return Mathf.Lerp(a, b, t);
        }

        public static float CircularIn(float a, float b, float t)
        {
            float c = b - a;
            return -c * (Mathf.Sqrt(1f - t * t) - 1f) + a;
        }

        public static float CircularOut(float a, float b, float t)
        {
            float c = b - a;
            t = t - 1f;
            return c * Mathf.Sqrt(1f - t * t) + a;
        }
    }
}
