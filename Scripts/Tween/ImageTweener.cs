﻿using UnityEngine;
using UnityEngine.UI;

namespace GameStack
{
    public static class TweenImageExtension
    {
        public static Coroutine TweenColor(this Image self, Color color, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.color, x => self.color = x, color, sec, easeType);
        }

        public static Coroutine TweenAlpha(this Image self, float alpha, float sec, EaseType easeType = EaseType.Linear)
        {
            Color color = self.color;
            color.a = alpha;
            return Tweener.Tween(() => self.color, x => self.color = x, color, sec, easeType);
        }
    }
}
