﻿using UnityEngine;

namespace GameStack
{
    public static class TransformTweener
    {
        public static Coroutine TweenMove(this Transform self, Vector3 position, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.position, x => self.position = x, position, sec, easeType);
        }

        public static Coroutine TweenMoveLocal(this Transform self, Vector3 position, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localPosition, x => self.localPosition = x, position, sec, easeType);
        }

        public static Coroutine TweenMoveDelta(this Transform self, Vector3 delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.position, x => self.position = x, self.position + delta, sec, easeType);
        }

        public static Coroutine TweenMoveDeltaLocal(this Transform self, Vector3 delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localPosition, x => self.localPosition = x, self.localPosition + delta, sec, easeType);
        }

        public static Coroutine TweenRotate(this Transform self, Quaternion rotation, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.rotation, x => self.rotation = x, rotation, sec, easeType);
        }

        public static Coroutine TweenRotateLocal(this Transform self, Quaternion rotation, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localRotation, x => self.localRotation = x, rotation, sec, easeType);
        }

        public static Coroutine TweenRotateDelta(this Transform self, Quaternion delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.rotation, x => self.rotation = x, self.rotation * delta, sec, easeType);
        }

        public static Coroutine TweenRotateDeltaLocal(this Transform self, Quaternion delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localRotation, x => self.localRotation = x, self.localRotation * delta, sec, easeType);
        }

        public static Coroutine TweenRotate(this Transform self, Vector3 eulerAngles, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.eulerAngles, x => self.eulerAngles = x, eulerAngles, sec, easeType);
        }

        public static Coroutine TweenRotateLocal(this Transform self, Vector3 eulerAngles, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localEulerAngles, x => self.localEulerAngles = x, eulerAngles, sec, easeType);
        }

        public static Coroutine TweenRotateDelta(this Transform self, Vector3 delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.eulerAngles, x => self.eulerAngles = x, self.eulerAngles + delta, sec, easeType);
        }

        public static Coroutine TweenRotateDeltaLocal(this Transform self, Vector3 delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localEulerAngles, x => self.localEulerAngles = x, self.localEulerAngles + delta, sec, easeType);
        }

        public static Coroutine TweenScaleLocal(this Transform self, Vector3 scale, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localScale, x => self.localScale = x, scale, sec, easeType);
        }

        public static Coroutine TweenScaleDeltaLocal(this Transform self, Vector3 delta, float sec, EaseType easeType = EaseType.Linear)
        {
            return Tweener.Tween(() => self.localScale, x => self.localScale = x, self.localScale + delta, sec, easeType);
        }
    }
}
