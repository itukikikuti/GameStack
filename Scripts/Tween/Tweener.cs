﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace GameStack
{
    public enum EaseType
    {
        Linear,
        CircularIn,
        CircularOut,
    }

    public static class Tweener
    {
        public delegate T Getter<out T>();
        public delegate void Setter<in T>(T value);

        private static Func<float, float, float, float>[] easings = {
            Easing.Linear,
            Easing.CircularIn,
            Easing.CircularOut,
        };

        private static float Ease(float a, float b, float t, EaseType easeType)
        {
            return easings[(int)easeType](a, b, t);
        }

        public static Coroutine Tween(Getter<float> getter, Setter<float> setter, float end, float sec, EaseType easeType)
        {
            return CoroutineRunner.RunCoroutine(RunTween(getter, setter, end, sec, easeType));
        }

        public static Coroutine Tween(Getter<Vector3> getter, Setter<Vector3> setter, Vector3 end, float sec, EaseType easeType)
        {
            return CoroutineRunner.RunCoroutine(RunTween(getter, setter, end, sec, easeType));
        }

        public static Coroutine Tween(Getter<Quaternion> getter, Setter<Quaternion> setter, Quaternion end, float sec, EaseType easeType)
        {
            return CoroutineRunner.RunCoroutine(RunTween(getter, setter, end, sec, easeType));
        }

        public static Coroutine Tween(Getter<Color> getter, Setter<Color> setter, Color end, float sec, EaseType easeType)
        {
            return CoroutineRunner.RunCoroutine(RunTween(getter, setter, end, sec, easeType));
        }

        private static IEnumerator RunTween(Getter<float> getter, Setter<float> setter, float end, float sec, EaseType easeType)
        {
            float start = getter();

            float t = 0f;
            while (t < sec)
            {
                setter(Ease(start, end, t / sec, easeType));

                t += Time.deltaTime;
                yield return null;
            }

            setter(end);
        }

        private static IEnumerator RunTween(Getter<Vector3> getter, Setter<Vector3> setter, Vector3 end, float sec, EaseType easeType)
        {
            Vector3 start = getter();

            float t = 0f;
            while (t < sec)
            {
                setter(Vector3.Lerp(start, end, Ease(0f, 1f, t / sec, easeType)));

                t += Time.deltaTime;
                yield return null;
            }

            setter(end);
        }

        private static IEnumerator RunTween(Getter<Quaternion> getter, Setter<Quaternion> setter, Quaternion end, float sec, EaseType easeType)
        {
            Quaternion start = getter();

            float t = 0f;
            while (t < sec)
            {
                setter(Quaternion.Lerp(start, end, Ease(0f, 1f, t / sec, easeType)));

                t += Time.deltaTime;
                yield return null;
            }

            setter(end);
        }

        private static IEnumerator RunTween(Getter<Color> getter, Setter<Color> setter, Color end, float sec, EaseType easeType)
        {
            Color start = getter();

            float t = 0f;
            while (t < sec)
            {
                setter(Color.Lerp(start, end, Ease(0f, 1f, t / sec, easeType)));

                t += Time.deltaTime;
                yield return null;
            }

            setter(end);
        }
    }
}
