﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace GameStack
{
    public class UI
    {
        public delegate Widget View(State state, Actions actions);
        public delegate void OnClickButton();
        public interface State { }
        public interface Actions { }

        public class Widget
        {
            internal class Component
            {
                public Type type = null;
                public Dictionary<string, object> parameters = null;

                public Component(Type type, Dictionary<string, object> parameters)
                {
                    this.type = type;
                    this.parameters = parameters;
                }
            }

            public string name = null;
            internal Component[] components = null;
            public Widget[] children = null;
            public Vector2 anchorMin = Vector2.zero;
            public Vector2 anchorMax = Vector2.one;
            public Vector2 offsetMin = Vector2.zero;
            public Vector2 offsetMax = Vector2.zero;
        }

        public static Sprite sprite = null;

        private Widget oldWidgetTree = null;
        private Widget newWidgetTree = null;
        private View view = null;
        private State state = null;
        private Actions actions = null;
        private GameObject root = null;

        public UI(View view, State state, Actions actions)
        {
            this.view = view;
            this.state = state;
            this.actions = actions;
            Resolve();
        }

        private void Resolve()
        {
            newWidgetTree = view(state, actions);
            SyncWidget(oldWidgetTree, newWidgetTree);

            oldWidgetTree = newWidgetTree;
        }

        private void SyncWidget(Widget oldWidget, Widget newWidget, Transform parent = null, int index = 0)
        {
            GameObject gameObject = SyncGameObject(oldWidget, newWidget, parent, index);

            SyncRectTransform(oldWidget, newWidget, gameObject, parent);

            for (int i = 0; i < newWidget.components.Length; i++)
            {
                SyncComponent(oldWidget?.components?[i], newWidget.components[i], gameObject, parent);
            }

            for (int i = 0; i < newWidget.children.Length; i++)
            {
                if (newWidget.children[i] != null)
                {
                    SyncWidget(oldWidget?.children?[i], newWidget.children[i], gameObject.transform, i);
                }
            }
        }

        private GameObject SyncGameObject(Widget oldWidget, Widget newWidget, Transform parent = null, int index = 0)
        {
            GameObject gameObject = null;

            if (oldWidget != null)
            {
                if (parent != null)
                    gameObject = parent.GetChild(index).gameObject;
                else
                    gameObject = root;
            }
            else
            {
                gameObject = new GameObject();
                gameObject.layer = LayerMask.NameToLayer("UI");

                if (parent == null)
                    root = gameObject;
            }

            gameObject.name = newWidget.name;

            return gameObject;
        }

        private void SyncRectTransform(Widget oldWidget, Widget newWidget, GameObject gameObject, Transform parent = null)
        {
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();

            if (rectTransform == null)
                rectTransform = gameObject.AddComponent<RectTransform>();

            if (oldWidget == null)
            {
                if (parent != null)
                {
                    rectTransform.SetParent(parent);

                    rectTransform.anchorMin = newWidget.anchorMin;
                    rectTransform.anchorMax = newWidget.anchorMax;
                    rectTransform.offsetMin = newWidget.offsetMin;
                    rectTransform.offsetMax = newWidget.offsetMax;
                }
            }
        }

        private void SyncComponent(Widget.Component oldComponent, Widget.Component newComponent, GameObject gameObject, Transform parent = null)
        {
            Component instance = gameObject.GetComponent(newComponent.type);

            if (instance == null)
                instance = gameObject.AddComponent(newComponent.type);

            foreach (var param in newComponent.parameters)
            {
                PropertyInfo property = newComponent.type.GetProperty(param.Key);

                if (param.Value is Delegate)
                {
                    if (param.Value is OnClickButton onClickButton)
                    {
                        Button.ButtonClickedEvent events = property.GetValue(instance) as Button.ButtonClickedEvent;
                        events.RemoveAllListeners();
                        events.AddListener(() =>
                        {
                            onClickButton();
                            Resolve();
                        });
                    }
                }
                else
                {
                    property.SetValue(instance, param.Value);
                }
            }
        }

        public static Widget Root(
            Widget child = null,
            Vector2? referenceResolution = null)
        {
            Widget widget = new Widget();
            widget.name = nameof(Root);
            widget.children = new[] { child };
            widget.components = new[]
            {
                new Widget.Component(
                    typeof(Canvas),
                    new Dictionary<string, object>
                    {
                        { "renderMode", RenderMode.ScreenSpaceOverlay }
                    }
                ),
                new Widget.Component(
                    typeof(CanvasScaler),
                    new Dictionary<string, object>
                    {
                        { "uiScaleMode", CanvasScaler.ScaleMode.ScaleWithScreenSize },
                        { "referenceResolution", referenceResolution ?? new Vector2(800f, 600f) }
                    }
                ),
                new Widget.Component(
                    typeof(GraphicRaycaster),
                    new Dictionary<string, object>()
                )
            };

            return widget;
        }

        public static Widget Image(
            Widget child = null,
            Sprite sprite = null,
            Color? color = null,
            bool? raycastTarget = null)
        {
            Widget widget = new Widget();
            widget.name = nameof(Image);
            widget.children = new[] { child };
            widget.components = new[]
            {
                new Widget.Component(
                    typeof(Image),
                    new Dictionary<string, object>
                    {
                        { "sprite", sprite ?? UI.sprite },
                        { "color", color ?? Color.white },
                        { "raycastTarget", raycastTarget ?? true },
                        { "type", UnityEngine.UI.Image.Type.Sliced }
                    }
                )
            };

            return widget;
        }

        public static Widget Text(
            string text,
            Font font = null,
            FontStyle? fontStyle = null,
            int? fontSize = null,
            float? lineSpacing = null,
            bool? supportRichText = null,
            TextAnchor? alignment = null,
            HorizontalWrapMode? horizontalOverflow = null,
            VerticalWrapMode? verticalOverflow = null,
            bool? resizeTextForBestFit = null,
            int? resizeTextMinSize = null,
            int? resizeTextMaxSize = null,
            Color? color = null,
            bool? raycastTarget = null)
        {
            Widget widget = new Widget();
            widget.name = nameof(Text);
            widget.children = new Widget[0];
            widget.components = new[]
            {
                new Widget.Component(
                    typeof(Text),
                    new Dictionary<string, object>
                    {
                        { "text", text },
                        { "font", font ?? Resources.GetBuiltinResource<Font>("Arial.ttf") },
                        { "fontStyle", fontStyle ?? FontStyle.Normal },
                        { "fontSize", fontSize ?? 14 },
                        { "lineSpacing", lineSpacing ?? 1f },
                        { "supportRichText", supportRichText ?? true },
                        { "alignment", alignment ?? TextAnchor.UpperLeft },
                        { "horizontalOverflow", horizontalOverflow ?? HorizontalWrapMode.Wrap },
                        { "verticalOverflow", verticalOverflow ?? VerticalWrapMode.Truncate },
                        { "resizeTextForBestFit", resizeTextForBestFit ?? false },
                        { "resizeTextMinSize", resizeTextMinSize ?? 0 },
                        { "resizeTextMaxSize", resizeTextMaxSize ?? 300 },
                        { "color", color ?? Color.black },
                        { "raycastTarget", raycastTarget ?? true }
                    }
                )
            };

            return widget;
        }

        public static Widget Button(
            Widget child = null,
            OnClickButton onClick = null)
        {
            Widget widget = new Widget();
            widget.name = nameof(Button);
            widget.children = new[] { child };
            widget.components = new[]
            {
                new Widget.Component(
                    typeof(Button),
                    new Dictionary<string, object>
                    {
                        { "onClick", onClick ?? (()=>{}) },
                    }
                )
            };

            return widget;
        }

        public static Widget GridLayout(
            Widget[] children = null,
            Vector2? cellSize = null,
            Vector2? spacing = null)
        {
            Widget widget = new Widget();
            widget.name = nameof(GridLayout);
            widget.children = children ?? new Widget[0];
            widget.components = new[]
            {
                new Widget.Component(
                    typeof(GridLayoutGroup),
                    new Dictionary<string, object>
                    {
                        { "cellSize", cellSize ?? new Vector2(100f, 100f) },
                        { "spacing", spacing ?? Vector2.zero }
                    }
                )
            };

            return widget;
        }

        public static Widget PaddingLayout(
            Widget child = null,
            float? left = null,
            float? bottom = null,
            float? right = null,
            float? top = null)
        {
            Widget widget = new Widget();
            widget.name = nameof(PaddingLayout);
            widget.children = new[] { child };
            widget.components = new Widget.Component[0];
            widget.anchorMin = Vector2.zero;
            widget.anchorMax = Vector2.one;
            widget.offsetMin = new Vector2(left ?? 0f, bottom ?? 0f);
            widget.offsetMax = -new Vector2(right ?? 0f, top ?? 0f);

            return widget;
        }
    }
}
