﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace GameStack
{
#if UNITY_EDITOR
    public class GizmoRenderer : SingletonBehaviour<GizmoRenderer>
    {
        public static void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            Instance.vertexs.Enqueue((start, end, color));
        }
        public static void DrawBox2D(Vector2 point, Vector2 size, float angle, Color color)
        {
            Vector2[] vertexs = {
                    new Vector2(size.x, size.y),
                    new Vector2(-size.x, size.y),
                    new Vector2(-size.x, -size.y),
                    new Vector2(size.x, -size.y)
                };

            for (int i = 0; i < vertexs.Length; i++)
            {
                vertexs[i] = vertexs[i] / 2f + point;
                vertexs[i] = RotatePoint(vertexs[i], point, Quaternion.Euler(0f, 0f, angle));
            }

            DrawLine(vertexs[0], vertexs[1], color);
            DrawLine(vertexs[1], vertexs[2], color);
            DrawLine(vertexs[2], vertexs[3], color);
            DrawLine(vertexs[3], vertexs[0], color);
        }
        public static void DrawBox3D(Vector3 center, Vector3 halfExtents, Quaternion orientation, Color color)
        {
            Vector3[] vertexs = {
                    new Vector3(halfExtents.x, halfExtents.y, halfExtents.z),
                    new Vector3(-halfExtents.x, halfExtents.y, halfExtents.z),
                    new Vector3(halfExtents.x, -halfExtents.y, halfExtents.z),
                    new Vector3(-halfExtents.x, -halfExtents.y, halfExtents.z),
                    new Vector3(halfExtents.x, halfExtents.y, -halfExtents.z),
                    new Vector3(-halfExtents.x, halfExtents.y, -halfExtents.z),
                    new Vector3(halfExtents.x, -halfExtents.y, -halfExtents.z),
                    new Vector3(-halfExtents.x, -halfExtents.y, -halfExtents.z)
                };

            for (int i = 0; i < vertexs.Length; i++)
            {
                vertexs[i] += center;
                vertexs[i] = RotatePoint(vertexs[i], center, orientation);
            }

            DrawLine(vertexs[0], vertexs[1], color);
            DrawLine(vertexs[2], vertexs[3], color);
            DrawLine(vertexs[4], vertexs[5], color);
            DrawLine(vertexs[6], vertexs[7], color);

            DrawLine(vertexs[0], vertexs[2], color);
            DrawLine(vertexs[1], vertexs[3], color);
            DrawLine(vertexs[4], vertexs[6], color);
            DrawLine(vertexs[5], vertexs[7], color);

            DrawLine(vertexs[0], vertexs[4], color);
            DrawLine(vertexs[1], vertexs[5], color);
            DrawLine(vertexs[2], vertexs[6], color);
            DrawLine(vertexs[3], vertexs[7], color);
        }
        public static void DrawCircle(Vector3 position, float radius, Quaternion orientation, bool half, Color color)
        {
            int edge = half ? 10 : 20;
            float fullAngle = half ? 180f : 360f;

            Vector3[] vertexs = new Vector3[edge + 1];

            for (int i = 0; i < vertexs.Length; i++)
            {
                float radian = (fullAngle / edge) * i * Mathf.Deg2Rad;
                vertexs[i] = new Vector3(Mathf.Cos(radian), Mathf.Sin(radian), 0f) * radius;
                vertexs[i] += position;
                vertexs[i] = RotatePoint(vertexs[i], position, orientation);
            }

            for (int i = 0; i < edge; i++)
            {
                DrawLine(vertexs[i], vertexs[i + 1], color);
            }
        }
        public static void DrawSphere(Vector3 position, float radius, Quaternion orientation, Color color)
        {
            DrawCircle(position, radius, orientation * Quaternion.Euler(0f, 0f, 0f), false, color);
            DrawCircle(position, radius, orientation * Quaternion.Euler(90f, 0f, 0f), false, color);
            DrawCircle(position, radius, orientation * Quaternion.Euler(0f, 90f, 0f), false, color);
        }
        public static void DrawCapsule3D(Vector3 start, Vector3 end, float radius, Quaternion orientation, Color color)
        {
            DrawCircle(start, radius, orientation * Quaternion.Euler(0f, 0f, 0f), false, color);
            DrawCircle(start, radius, orientation * Quaternion.Euler(90f, 0f, 0f), true, color);
            DrawCircle(start, radius, orientation * Quaternion.Euler(0f, 90f, 90f), true, color);

            DrawCircle(end, radius, orientation * Quaternion.Euler(0f, 0f, 0f), false, color);
            DrawCircle(end, radius, orientation * Quaternion.Euler(-90f, 0f, 0f), true, color);
            DrawCircle(end, radius, orientation * Quaternion.Euler(0f, -90f, 90f), true, color);

            Vector3[] vertexs = {
                    new Vector3(radius, 0f, 0f),
                    new Vector3(-radius, 0f, 0f),
                    new Vector3(0f, radius, 0f),
                    new Vector3(0f, -radius, 0f)
                };

            for (int i = 0; i < vertexs.Length; i++)
            {
                Vector3 temp1 = RotatePoint(vertexs[i] + start, start, orientation);
                Vector3 temp2 = RotatePoint(vertexs[i] + end, end, orientation);
                DrawLine(temp1, temp2, color);
            }
        }
        private static Vector3 RotatePoint(Vector3 point, Vector3 pivot, Quaternion orientation)
        {
            Vector3 dirction = point - pivot;
            dirction = orientation * dirction;
            return dirction + pivot;
        }

        public Queue<(Vector3, Vector3, Color)> vertexs = new Queue<(Vector3, Vector3, Color)>();

        private void OnDrawGizmos()
        {
            while (vertexs.Count > 0)
            {
                var vertex = vertexs.Dequeue();
                Gizmos.color = vertex.Item3;
                Gizmos.DrawLine(vertex.Item1, vertex.Item2);
            }
        }
    }
#endif

    public static class VisiblePhysics
    {
        public static bool Linecast(Vector3 start, Vector3 end, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.Linecast(
                start,
                end,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = isHit ? Color.red : Color.green;
            GizmoRenderer.DrawLine(start, end, color);
#endif

            return isHit;
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            return Raycast(
                origin,
                direction,
                out _,
                maxDistance,
                layerMask,
                queryTriggerInteraction
                );
        }
        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.Raycast(
                origin,
                direction,
                out hitInfo,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            if (isHit)
                GizmoRenderer.DrawLine(origin, hitInfo.point, Color.red);
            else
                GizmoRenderer.DrawLine(origin, origin + direction * (maxDistance ?? 10000f), Color.green);
#endif

            return isHit;
        }
        public static RaycastHit[] RaycastAll(Vector3 origin, Vector3 direction, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            RaycastHit[] hitInfos = Physics.RaycastAll(
                origin,
                direction,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = hitInfos.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawLine(origin, origin + direction * (maxDistance ?? 10000f), color);
#endif

            return hitInfos;
        }

        public static bool CheckBox(Vector3 center, Vector3 halfExtents, Quaternion? orientation = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.CheckBox(
                center,
                halfExtents,
                orientation ?? Quaternion.identity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = isHit ? Color.red : Color.green;
            GizmoRenderer.DrawBox3D(center, halfExtents, orientation ?? Quaternion.identity, color);
#endif

            return isHit;
        }
        public static Collider[] OverlapBox(Vector3 center, Vector3 halfExtents, Quaternion? orientation = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            Collider[] colliders = Physics.OverlapBox(
                center,
                halfExtents,
                orientation ?? Quaternion.identity,
                layerMask ?? Physics.AllLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = colliders.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawBox3D(center, halfExtents, orientation ?? Quaternion.identity, color);
#endif

            return colliders;
        }
        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction, Quaternion? orientation = null, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            return BoxCast(
                center,
                halfExtents,
                direction,
                out _,
                orientation,
                maxDistance,
                layerMask,
                queryTriggerInteraction
                );
        }
        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction, out RaycastHit hitInfo, Quaternion? orientation = null, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.BoxCast(
                center,
                halfExtents,
                direction,
                out hitInfo,
                orientation ?? Quaternion.identity,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            if (isHit)
            {
                GizmoRenderer.DrawLine(center, center + direction * hitInfo.distance, Color.red);
                GizmoRenderer.DrawBox3D(center + direction * hitInfo.distance, halfExtents, Quaternion.LookRotation(direction) * (orientation ?? Quaternion.identity), Color.red);
            }
            else
                GizmoRenderer.DrawLine(center, center + direction * (maxDistance ?? 10000f), Color.green);
#endif

            return isHit;
        }
        public static RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents, Vector3 direction, Quaternion? orientation = null, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            RaycastHit[] hitInfos = Physics.BoxCastAll(
                center,
                halfExtents,
                direction,
                orientation ?? Quaternion.identity,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = hitInfos.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawLine(center, center + direction * (maxDistance ?? 10000f), color);

            foreach (var hitInfo in hitInfos)
            {
                GizmoRenderer.DrawBox3D(center + direction * hitInfo.distance, halfExtents, Quaternion.LookRotation(direction) * (orientation ?? Quaternion.identity), Color.red);
            }
#endif

            return hitInfos;
        }

        public static bool CheckCapsule(Vector3 start, Vector3 end, float radius, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.CheckCapsule(
                start,
                end,
                radius,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = isHit ? Color.red : Color.green;
            GizmoRenderer.DrawCapsule3D(start, end, radius, Quaternion.LookRotation(start - end), color);
#endif

            return isHit;
        }
        public static Collider[] OverlapCapsule(Vector3 start, Vector3 end, float radius, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            Collider[] colliders = Physics.OverlapCapsule(
                start,
                end,
                radius,
                layerMask ?? Physics.AllLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = colliders.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawCapsule3D(start, end, radius, Quaternion.LookRotation(start - end), color);
#endif

            return colliders;
        }
        public static bool CapsuleCast(Vector3 start, Vector3 end, float radius, Vector3 direction, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            return CapsuleCast(
                start,
                end,
                radius,
                direction,
                out _,
                maxDistance,
                layerMask,
                queryTriggerInteraction
                );
        }
        public static bool CapsuleCast(Vector3 start, Vector3 end, float radius, Vector3 direction, out RaycastHit hitInfo, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.CapsuleCast(
                start,
                end,
                radius,
                direction,
                out hitInfo,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            if (isHit)
            {
                GizmoRenderer.DrawLine(start, start + direction * hitInfo.distance, Color.red);
                GizmoRenderer.DrawLine(end, end + direction * hitInfo.distance, Color.red);
                GizmoRenderer.DrawCapsule3D(start + direction * hitInfo.distance, end + direction * hitInfo.distance, radius, Quaternion.LookRotation(start - end), Color.red);
            }
            else
            {
                GizmoRenderer.DrawLine(start, start + direction * (maxDistance ?? 10000f), Color.green);
                GizmoRenderer.DrawLine(end, end + direction * (maxDistance ?? 10000f), Color.green);
            }
#endif

            return isHit;
        }
        public static RaycastHit[] CapsuleCastAll(Vector3 start, Vector3 end, float radius, Vector3 direction, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            RaycastHit[] hitInfos = Physics.CapsuleCastAll(
                start,
                end,
                radius,
                direction,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = hitInfos.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawLine(start, start + direction * (maxDistance ?? 10000f), color);
            GizmoRenderer.DrawLine(end, end + direction * (maxDistance ?? 10000f), color);

            foreach (var hitInfo in hitInfos)
            {
                GizmoRenderer.DrawCapsule3D(start + direction * hitInfo.distance, end + direction * hitInfo.distance, radius, Quaternion.LookRotation(start - end), Color.red);
            }
#endif

            return hitInfos;
        }

        public static bool CheckSphere(Vector3 position, float radius, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.CheckSphere(
                position,
                radius,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = isHit ? Color.red : Color.green;
            GizmoRenderer.DrawSphere(position, radius, Quaternion.identity, color);
#endif

            return isHit;
        }
        public static Collider[] OverlapSphere(Vector3 position, float radius, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            Collider[] colliders = Physics.OverlapSphere(
                position,
                radius,
                layerMask ?? Physics.AllLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = colliders.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawSphere(position, radius, Quaternion.identity, color);
#endif
            return colliders;
        }
        public static bool SphereCast(Vector3 position, float radius, Vector3 direction, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            return SphereCast(
                position,
                radius,
                direction,
                out _,
                maxDistance,
                layerMask,
                queryTriggerInteraction
                );
        }
        public static bool SphereCast(Vector3 position, float radius, Vector3 direction, out RaycastHit hitInfo, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            bool isHit = Physics.SphereCast(
                position,
                radius,
                direction,
                out hitInfo,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            if (isHit)
            {
                GizmoRenderer.DrawLine(position, position + direction * hitInfo.distance, Color.red);
                GizmoRenderer.DrawSphere(position + direction * hitInfo.distance, radius, Quaternion.LookRotation(direction), Color.red);
            }
            else
                GizmoRenderer.DrawLine(position, position + direction * (maxDistance ?? 10000f), Color.green);
#endif

            return isHit;
        }
        public static RaycastHit[] SphereCastAll(Vector3 position, float radius, Vector3 direction, float? maxDistance = null, int? layerMask = null, QueryTriggerInteraction? queryTriggerInteraction = null)
        {
            RaycastHit[] hitInfos = Physics.SphereCastAll(
                position,
                radius,
                direction,
                maxDistance ?? Mathf.Infinity,
                layerMask ?? Physics.DefaultRaycastLayers,
                queryTriggerInteraction ?? QueryTriggerInteraction.UseGlobal
                );

#if UNITY_EDITOR
            Color color = hitInfos.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawLine(position, position + direction * (maxDistance ?? 10000f), color);

            foreach (var hitInfo in hitInfos)
            {
                GizmoRenderer.DrawSphere(position + direction * hitInfo.distance, radius, Quaternion.LookRotation(direction), Color.red);
            }
#endif

            return hitInfos;
        }
    }

    public static class VisiblePhysics2D
    {
        public static Collider2D OverlapPoint(Vector2 point, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            Collider2D collider = Physics2D.OverlapPoint(
                point,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            Color color = collider ? Color.red : Color.green;
            GizmoRenderer.DrawLine(point + Vector2.up * 0.1f, point + Vector2.down * 0.1f, color);
            GizmoRenderer.DrawLine(point + Vector2.right * 0.1f, point + Vector2.left * 0.1f, color);
#endif

            return collider;
        }
        public static Collider2D[] OverlapPointAll(Vector2 point, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            Collider2D[] colliders = Physics2D.OverlapPointAll(
                point,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            Color color = colliders.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawLine(point + Vector2.up * 0.1f, point + Vector2.down * 0.1f, color);
            GizmoRenderer.DrawLine(point + Vector2.right * 0.1f, point + Vector2.left * 0.1f, color);
#endif

            return colliders;
        }

        public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            Collider2D collider = Physics2D.OverlapArea(
                pointA,
                pointB,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            Color color = collider ? Color.red : Color.green;
            Vector2 point = (pointA + pointB) / 2f;
            Vector2 size = pointB - pointA;
            GizmoRenderer.DrawBox2D(point, size, 0f, color);
#endif

            return collider;
        }
        //public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);

        //public static RaycastHit2D Linecast(Vector2 start, Vector2 end, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
        //public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);

        //public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, [Internal.DefaultValue("Mathf.Infinity")] float distance, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
        //public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, [Internal.DefaultValue("Mathf.Infinity")] float distance, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);

        public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            Collider2D collider = Physics2D.OverlapBox(
                point,
                size,
                angle,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            Color color = collider ? Color.red : Color.green;
            GizmoRenderer.DrawBox2D(point, size, angle, color);
#endif

            return collider;
        }
        public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            Collider2D[] colliders = Physics2D.OverlapBoxAll(
                point,
                size,
                angle,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            Color color = colliders.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawBox2D(point, size, angle, color);
#endif

            return colliders;
        }

        public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction, float? distance = null, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            RaycastHit2D hit = Physics2D.BoxCast(
                origin,
                size,
                angle,
                direction,
                distance ?? Mathf.Infinity,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            if (hit.collider)
            {
                GizmoRenderer.DrawLine(origin, origin + direction * hit.distance, Color.red);
                GizmoRenderer.DrawBox2D(origin + direction * hit.distance, size, angle, Color.red);
            }
            else
                GizmoRenderer.DrawLine(origin, origin + direction * (distance ?? 10000f), Color.green);
#endif

            return hit;
        }
        public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle, Vector2 direction, float? distance = null, int? layerMask = null, float? minDepth = null, float? maxDepth = null)
        {
            RaycastHit2D[] hits = Physics2D.BoxCastAll(
                origin,
                size,
                angle,
                direction,
                distance ?? Mathf.Infinity,
                layerMask ?? Physics2D.DefaultRaycastLayers,
                minDepth ?? -Mathf.Infinity,
                maxDepth ?? Mathf.Infinity
                );

#if UNITY_EDITOR
            Color color = hits.Length > 0 ? Color.red : Color.green;
            GizmoRenderer.DrawLine(origin, origin + direction * (distance ?? 10000f), color);

            foreach (var hit in hits)
            {
                GizmoRenderer.DrawBox2D(origin + direction * hit.distance, size, angle, Color.red);
            }
#endif

            return hits;
        }

        //public static Collider2D OverlapCapsule(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
        //public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);

        //public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, [Internal.DefaultValue("Mathf.Infinity")] float distance, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
        //public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, [Internal.DefaultValue("Mathf.Infinity")] float distance, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);

        //public static Collider2D OverlapCircle(Vector2 point, float radius, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
        //public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);

        //public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction, [Internal.DefaultValue("Mathf.Infinity")] float distance, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
        //public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction, [Internal.DefaultValue("Mathf.Infinity")] float distance, [Internal.DefaultValue("DefaultRaycastLayers")] int layerMask, [Internal.DefaultValue("-Mathf.Infinity")] float minDepth, [Internal.DefaultValue("Mathf.Infinity")] float maxDepth);
    }
}
