﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace GameStack
{
    public static class NativeInput
    {
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
        private class NativeInputHandler : SingletonBehaviour<NativeInputHandler>
        {
            [SerializeField]
            private IntPtr windowHandle = IntPtr.Zero;
            [SerializeField]
            private IntPtr oldWindowProcedurePtr = IntPtr.Zero;
            [SerializeField]
            private IntPtr newWindowProcedurePtr = IntPtr.Zero;
            [SerializeField]
            private WindowProcedure newWindowProcedure = null;
            [SerializeField, HideInInspector]
            public int wheelDelta = 0;

            protected override void Awake()
            {
                base.Awake();

                windowHandle = FindWindow(null, "GameStack");
                newWindowProcedure = new WindowProcedure(OnProceedWindow);
                newWindowProcedurePtr = Marshal.GetFunctionPointerForDelegate(newWindowProcedure);
                oldWindowProcedurePtr = SetWindowLongPtr(windowHandle, -4, newWindowProcedurePtr);
            }

            private void LateUpdate()
            {
                wheelDelta = 0;
            }

            private void OnApplicationQuit()
            {
                SetWindowLongPtr(windowHandle, -4, oldWindowProcedurePtr);
                oldWindowProcedurePtr = IntPtr.Zero;
                newWindowProcedurePtr = IntPtr.Zero;
                newWindowProcedure = null;
                windowHandle = IntPtr.Zero;
            }

            private IntPtr OnProceedWindow(IntPtr windowHandle, WindowMessage windowMessage, UIntPtr wParam, IntPtr lParam)
            {
                if (windowMessage == WindowMessage.MouseWheel)
                    wheelDelta = (Int32)wParam >> 16;

                return CallWindowProc(oldWindowProcedurePtr, windowHandle, windowMessage, wParam, lParam);
            }
        }

        private enum WindowMessage : uint
        {
            MouseWheel = 0x020A
        }

        private delegate IntPtr WindowProcedure(IntPtr windowHandle, WindowMessage windowMessage, UIntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll", EntryPoint = "FindWindowW", CharSet = CharSet.Unicode)]
        private static extern IntPtr FindWindow(
            [MarshalAs(UnmanagedType.LPWStr), In] string className,
            [MarshalAs(UnmanagedType.LPWStr), In] string windowName
        );

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr SetWindowLongPtr(
            IntPtr windowHandle,
            int index,
            IntPtr replacement
        );

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        private static extern IntPtr CallWindowProc(
            IntPtr prevWindowProcedure,
            IntPtr windowHandle,
            WindowMessage windowMessage,
            UIntPtr wParam,
            IntPtr lParam
        );

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            Debug.Log($"{NativeInputHandler.Instance.name} initialized");
        }
#endif

        public static float GetMouseWheel()
        {
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
            return NativeInputHandler.Instance.wheelDelta / 1200f;
#else
            return Input.GetAxis("Mouse ScrollWheel");
#endif
        }
    }
}
